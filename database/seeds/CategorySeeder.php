<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content_category')->insert([
            'category_code' => 'video',
            'category_name' => 'Video',
            'category_parent_code' => '',
            'category_icon' => '',
            'category_active' => true
        ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'games',
        //     'category_name' => 'Games',
        //     'category_parent_code' => '',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'sharing',
        //     'category_name' => 'Sharing',
        //     'category_parent_code' => '',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'general',
        //     'category_name' => 'General',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'safety',
        //     'category_name' => 'Safety',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'service',
        //     'category_name' => 'Service',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'operation',
        //     'category_name' => 'Operation',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'human_capital',
        //     'category_name' => 'Human Capital',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'tech',
        //     'category_name' => 'IT',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'procurement',
        //     'category_name' => 'Procurement',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'commercial',
        //     'category_name' => 'Commercial',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'support',
        //     'category_name' => 'Support',
        //     'category_parent_code' => 'sharing',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'gapedia',
        //     'category_name' => 'GA Pedia',
        //     'category_parent_code' => '',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'event_training',
        //     'category_name' => 'Event/Training',
        //     'category_parent_code' => '',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);

        // DB::table('content_category')->insert([
        //     'category_code' => 'community',
        //     'category_name' => 'Community',
        //     'category_parent_code' => '',
        //     'category_icon' => '',
        //     'category_active' => true
        // ]);
    }
}
