<?php

namespace App\Http\Controllers;

use App\ContentCategory;
use Illuminate\Http\Request;

class MasterController extends Controller
{

    public function getCategories()
    {
        $categories = ContentCategory::where([
            'category_active' => true,
            'category_parent_code' => null,
        ])->get();
        return response()->json($categories);
    }

    public function getSubCategory(Request $request)
    {
        $param = $request->all();
        $categories = ContentCategory::where([
            'category_active' => true,
            'category_parent_code' => $param['category'],
        ])->get();
        return response()->json($categories);
    }

    public function newCategory(Request $request)
    {
        $param = $request->all();

        $checkCategory = ContentCategory::where('category_code', $param['code'])->first();
        if ($checkCategory != null && $param['update'] == null) {
            return response()->json(['error' => 'Category exist']);
        }

        $category = [
            'category_code' => $param['code'],
            'category_name' => $param['name'],
            'category_parent_code' => $param['parent'],
            'category_icon' => $param['icon'],
            'category_active' => true
        ];

        $store = ContentCategory::create($category);

        return response()->json($store);
    }
}
