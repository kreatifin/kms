<?php

namespace App\Http\Controllers;

use App\ContentPost;
use App\ContentCategory;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function checkSubCategory(Request $request)
    {
        $param = $request->all();

        $category = ContentCategory::where('category_parent_code', $param['category'])->first();
        if ($category == null) {
            return response()->json(['status' => 'no_content']);
        } else {
            return response()->json(['status' => 'sub_content']);
        }
    }

    public function contentList()
    {
        $posts = ContentPost::where('post_active', true)->orderBy('created_at', 'desc')->get();
        return response()->json($posts);
    }

    public function contentNew(Request $request)
    {
        $param = $request->all();

        $newContent = [
            'post_title' => $param['title'],
            'post_author' => $param['author'],
            'post_content' => $param['content'],
            'post_category' => $param['category']
        ];

        $store = ContentPost::create($newContent);

        return response()->json($store);
    }


}
