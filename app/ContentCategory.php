<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentCategory extends Model
{
    protected $table = 'content_category';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
}
