<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentPost extends Model
{
    protected $table = "content_posts";

    protected $guarded = ['id', 'created_at', 'updated_at'];
}
